#include <FS.h>                   //this needs to be first, or it all crashes and burns...
/*
Use DHT to measure moisture and temperature
Display it on 128 * 128 SPI TFT LCD
Send it to MQTT server
Use WiFiManager to connect to WiFi

 */
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <PubSubClient.h>
//needed for library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson

#include <SPI.h>
#include <TFT_ILI9163C.h>
#include "_fonts/unborn_small.c"
#include "DHT.h"
/*
 fonts are inside _fonts folder and should be declared here
 notice that default font it's arial x2.c and it's always loaded!
 You can use only fonts prepared to be used with sumotoy libraries
 that uses LPGO Text rendering like:
https://github.com/sumotoy/TFT_ILI9163C/tree/Pre-Release-1.0r4
https://github.com/sumotoy/RA8875/tree/0.70b11
https://github.com/sumotoy/OLED_pictivaWide
*/

#define __CS  16  //(D0)
#define __DC  5   //(D1)
#define __RST 4

#define DHTPIN 12     // what digital pin we're connected to

// Uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11

DHT dht(DHTPIN, DHTTYPE);
TFT_ILI9163C tft = TFT_ILI9163C(__CS, __DC);

const char *mqtt_server = "192.168.0.37";
const char *mqtt_port = "1883";
const char *mqtt_user = "";
const char *mqtt_password = "";

const char *clientName = "esp/2";
const char *mqtt_name = "esp/2";
const char *commandName = "a1.s2.command";

//flag for saving data
bool shouldSaveConfig = false;

//callback notifying us of the need to save config
void saveConfigCallback () {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}

String stempC = "";
String shumid = "";
String mTemp = "cr/temp" ;
String mHumid = "cr/humidity" ;
int receivedValues = 0;
String prevReceivedSwitchValue = "";

unsigned long time1, time2;
int postinterval = 10000; // how often send MQTT if not changed?

const int maxlength = 100;
char message_buff[maxlength]; // max payload length
char topic_buff[maxlength];   // max topic length

WiFiClient espClient;
PubSubClient client(espClient);
char myIpString[24];

byte count = 1;


void mqtt_publish(String mqtt_topic, char* payload, byte length, byte retained) {
  
    char spayload[maxlength];

    if (mqtt_topic.length() > maxlength - 1) {
        payload = "topic too long! ";
        mqtt_topic = String(mqtt_name) + "/error";
        retained = byte(0);
    }
    mqtt_topic.toCharArray(topic_buff, mqtt_topic.length()+1);

    byte* p = (byte*)malloc(length);
    // Copy the payload to the new buffer
    memcpy(p,payload,length);

    client.publish(topic_buff, p, length, retained);
    // Free the memory;
    free(p);
}

void mqtt_string_publish(String mqtt_string, String mqtt_topic, String payload) {
    // uses global mqtt_name constant
  
    char cpayload[maxlength];
    String mqtt_topic2;
    byte debug = 0;
    if (payload.length() > maxlength - 1) {
      payload = "invalid payload provided: too long!"; 
      mqtt_topic2 = String(mqtt_name) + "/error";
      debug = 1;
    } else {
         mqtt_topic2 = String(mqtt_name) + "/"+ mqtt_string + "/" + mqtt_topic; 
    }
    payload.toCharArray(cpayload,payload.length()+1);
    if (debug == 1) {
      mqtt_publish(mqtt_topic2, cpayload, payload.length(), 0);
    } else {
     mqtt_publish(mqtt_topic2, cpayload, payload.length(), 1); 
    }
}

void mqtt_command_publish(String payload) {
    // uses global mqtt_name constant
  
    String mqtt_topic = String(commandName);
    char cpayload[maxlength];
    if (payload.length() > maxlength - 1) {
      payload = "invalid payload provided: too long!"; 
      mqtt_topic = mqtt_topic + "/error";
    } 
    payload.toCharArray(cpayload,payload.length()+1);
    mqtt_publish(mqtt_topic, cpayload, payload.length(), 0);
    // free(cpayload);
}



String getDigits(String digit){
 if(digit.toInt() < 10)
   digit = "0" + digit;
   return digit;
}

String myTime(){  
  int days = 0;
  int hours = 0;
  int mins = 0;
  int secs = millis() / 1000;
  
  mins = secs/60; //convert seconds to minutes
  hours = mins/60; //convert minutes to hours
  days = hours/24; //convert hours to days
  secs = secs - (mins*60); //subtract the coverted seconds to minutes in order to display 59 secs max 
  mins = mins - (hours*60); //subtract the coverted minutes to hours in order to display 59 minutes max
  hours = hours - (days*24); //subtract the coverted hours to days in order to display 23 hours max

  String myTime = "";
  if(days > 0) { myTime = myTime + getDigits(String(days)) + "d";} 
  myTime = myTime + getDigits(String(hours)) + ":" + getDigits(String(mins)) + ":" +   getDigits(String(secs));  
  
  return myTime;
 
}

void showTempHumi() {
  
  tft.setCursor(0, 0);
  tft.setFont(&unborn_small);//this will load the font
  tft.setTextScale(1);//smaller
  tft.setTextColor(WHITE, BLUE); //set a background inside font 
  tft.println("Temperatuur / Vocht");  
  tft.setTextColor(BLUE, BLACK); //set a background inside font
  tft.setTextScale(2);  
  tft.println("");  
  
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    tft.println("Failed to read from DHT sensor!");
    return;
  }

  tft.println("Vocht: ");
  tft.setTextColor(RED, BLACK); //set a background inside font
  tft.print(h);
  tft.println(" %");
  tft.println("");
  tft.setTextColor(BLUE, BLACK); //set a background inside font
  tft.println("Temp: ");
  tft.setTextColor(RED, BLACK); //set a background inside font
  tft.print(t);
  tft.println(" *C ");
  tft.setTextColor(BLUE, BLACK); //set a background inside font

  stempC = String(t);
  shumid = String(h);
  mTemp = "cr/1/temp" ;
  mHumid = "cr/1/humidity" ;
  receivedValues = 1;

}


void setup() {
  Serial.begin(115200);
  Serial.print("\nstarted\n");

//we dont use rst, instead of tying it to 3v3 we set it to high:
  pinMode(__RST, OUTPUT);
  digitalWrite(__RST, HIGH);

  
  tft.begin();
  /*
   after this command the library has already set several
   things as default:
   tft.clearScreen();//screen cleared with black
   tft.setRotation(0);//no rotation
   tft.setTextScale(1);//no scale
   tft.setCursor(0,0);
   tft.setTextWrap(true);//text will wrap to a new line automatically
   tft.setTextColor(WHITE,WHITE);//background transparent
   //default internal font loaded arial_x2
   This allow you to start writing immediately without
   need to set anything.
   */
  tft.setFont(&unborn_small);//this will load the font
  tft.setTextScale(1);//smaller
  tft.setTextColor(WHITE, BLUE); //set a background inside font 
  tft.println("Temperatuur / Vocht");  
  tft.setTextColor(BLUE, BLACK); //set a background inside font
  tft.println("");  
  tft.println("DHTxx test!");
  tft.clearScreen();
  dht.begin();

  //read configuration from FS json
  tft.println("mounting FS...");

  if (SPIFFS.begin()) {
    tft.println("mounted file system");
    if (SPIFFS.exists("/config.json")) {
      //file exists, reading and loading
      tft.println("reading config file");
      File configFile = SPIFFS.open("/config.json", "r");
      if (configFile) {
        tft.println("opened config file");
        size_t size = configFile.size();
        // Allocate a buffer to store contents of the file.
        std::unique_ptr<char[]> buf(new char[size]);

        configFile.readBytes(buf.get(), size);
        StaticJsonBuffer<200> jsonBuffer;
        JsonObject& json = jsonBuffer.parseObject(buf.get());
        json.printTo(Serial);
        if (json.success()) {
          Serial.println("\nparsed json");
          mqtt_server = json["mqtt_server"];
          mqtt_port = json["mqtt_port"];       
          mqtt_user = json["mqtt_user"];
          mqtt_password = json["mqtt_password"];  
        } else {
          tft.println("failed to load json config");
        }
      }
    }
  } else {
    tft.println("failed to mount FS");
  }
  //end read

  // The extra parameters to be configured (can be either global or just in the setup)
  // After connecting, parameter.getValue() will get you the configured value
  // id/name placeholder/prompt default length
  WiFiManagerParameter custom_mqtt_server("server", "MQTT server", mqtt_server, 40);
  WiFiManagerParameter custom_mqtt_port("port", "MQTT server port", mqtt_port, 40);  
  WiFiManagerParameter custom_mqtt_user("mqtt_user", "MQTT user", mqtt_user, 40);
  WiFiManagerParameter custom_mqtt_password("mqtt_password ", "MQTT password", mqtt_password, 40);

  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
  
  //set config save notify callback
  wifiManager.setSaveConfigCallback(saveConfigCallback);
  
  //add all your parameters here
  wifiManager.addParameter(&custom_mqtt_server);
  wifiManager.addParameter(&custom_mqtt_port);  
  wifiManager.addParameter(&custom_mqtt_user);      
  wifiManager.addParameter(&custom_mqtt_password);
 
  //reset settings - for testing
  //wifiManager.resetSettings();

  if (!wifiManager.autoConnect("AutoConnectAP")) {
    tft.println("failed to connect and hit timeout");
    delay(3000);
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(5000);
  }

  //if you get here you have connected to the WiFi
  tft.println("connected...yeey :)");

  //read updated parameters
  mqtt_server = custom_mqtt_server.getValue();
  mqtt_port = custom_mqtt_port.getValue();
  mqtt_user =  custom_mqtt_user.getValue();
  mqtt_password =  custom_mqtt_password.getValue();

  //save the custom parameters to FS
  if (shouldSaveConfig) {
    tft.println("Saving config");
    StaticJsonBuffer<200> jsonBuffer;
    JsonObject& json = jsonBuffer.createObject();
    json["mqtt_server"] = mqtt_server;
    json["mqtt_port"] =  mqtt_port;
    json["mqtt_user"] = mqtt_user;
    json["mqtt_password"] =  mqtt_password;   

    File configFile = SPIFFS.open("/config.json", "w");
    if (!configFile) {
      tft.println("Failed to open config file for writing");
    }

    json.printTo(Serial);
    json.printTo(configFile);
    configFile.close();
    //end save
}

  IPAddress myIp = WiFi.localIP();
  sprintf(myIpString, "%d.%d.%d.%d", myIp[0], myIp[1], myIp[2], myIp[3]);
  tft.print("IP: ");
  tft.println(myIpString);
  if ( MDNS.begin(mqtt_name) )  {
    tft.println ( "MDNS responder started" );
  }
  client.setServer(mqtt_server, 1883);
  mqtt_string_publish("debug","myIP",String(myIpString));
  mqtt_string_publish("debug","alive","1");
  mqtt_string_publish("debug","alive","startup");
  
  tft.println(myTime());
  tft.println("Startup complete!");
  mqtt_string_publish("debug", "uptime", myTime());
  tft.clearScreen();
  
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");

    if (client.connect(mqtt_name)) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
  IPAddress myIp = WiFi.localIP();
  sprintf(myIpString, "%d.%d.%d.%d", myIp[0], myIp[1], myIp[2], myIp[3]);  
  mqtt_string_publish("debug", "ip", myIpString);
}

void loop() {
    if (!client.connected()) {
      reconnect();
    }
    if (millis() > time1 + postinterval) {
      showTempHumi();
      time1 = millis();
      Serial.print("uptime: ");
      Serial.println(myTime());
      mqtt_string_publish("debug", "uptime", myTime());
      mqtt_string_publish("data", mTemp , stempC); 
      mqtt_string_publish("data", mHumid, shumid);       

    } 
   
  

   // Wait a few seconds between measurements.

  

}
